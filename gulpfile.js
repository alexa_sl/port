var os = require('os');
var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var wireDepStream = require('wiredep').stream;
var $ = require('gulp-load-plugins')();
var del = require('del');
var rimraf = require('rimraf');
var runSequence = require('run-sequence');

var folders = {
    assets: 'assets'
};
var buildFolder = 'build';

var plumberConfig = {
    errorHandler: function (err) {
        $.util.log($.util.colors.red(err.toString()));
        $.util.beep();
    }
};

var browserApp = (function () {
    //also can be 'firefox'
    var platformBrowserApp;

    switch (os.platform()) {
        case 'linux':
            platformBrowserApp = 'google-chrome';
            break;
        case 'darwin':
            platformBrowserApp = 'open /Applications/Google\\ Chrome.app';
            break;
        case 'win32':
            platformBrowserApp = 'chrome';
            break;
        default:
            $.util.log($.util.colors.red('Unsupported dev platform'));
            process.exit();
            break;
    }

    return platformBrowserApp;
})();


gulp.task('default', ['watchers']);

gulp.task('build', function () {
    'use strict';
    runSequence('clean', ['process-js-app', 'process-index', 'process-js-vendor', 'process-less', 'process-favicon', 'process-images'], 'open-browser');
});


gulp.task('clean', function () {
    'use strict';

    return gulp.src([
        path.join(buildFolder, '*'),
        path.join(buildFolder, 'js'),
        path.join(buildFolder, 'css'),
        path.join(buildFolder, 'images')
    ], { read: false })
        .pipe($.rimraf());
});

gulp.task('process-images', function () {
    'use strict';

    return gulp.src(path.join(folders.assets, 'images', '*'))
        .pipe(gulp.dest(path.join(buildFolder, 'images')));
});

gulp.task('process-favicon', function () {
    'use strict';

    return gulp.src(path.join(folders.assets, 'favicon', '*', '*.*'))
        .pipe(gulp.dest(buildFolder));
});

gulp.task('process-index', function () {
    'use strict';

    return gulp.src('index.html')
        .pipe($.useref())
        .pipe(gulp.dest(buildFolder));
});
gulp.task('process-fonts', function () {
    'use strict';

    return gulp.src(path.join(folders.assets, 'fonts', '*', '*.*'))
        .pipe(gulp.dest(buildFolder, 'fonts'));
});

gulp.task('process-less', function () {
    'use strict';

    return gulp.src(path.join(folders.assets, 'styles', 'build.less'))
        .pipe($.plumber(plumberConfig))
        .pipe($.less())
        .pipe($.cssimport())
        .pipe($.autoprefixer())
        .pipe($.size({title: 'Original styles size: '}))
        //.pipe($.csso())
        .pipe($.size({title: 'Compressed styles size: '}))
        .pipe($.rename({
            basename: 'all'
        }))
        .pipe(gulp.dest(path.join(buildFolder, 'css')));
});

gulp.task('process-js-app', function () {
    'use strict';

    return gulp.src('index.html')
        .pipe($.useref.assets())
        .pipe($.filter(function (file) {
            return /app\.js$/.test(file.path);
        }))
        .pipe(gulp.dest(buildFolder));
});

gulp.task('process-js-vendor', function () {
    'use strict';

    return gulp.src('index.html')
        .pipe($.useref.assets())
        .pipe($.filter(function (file) {
            return /vendors\.js$/.test(file.path);
        }))
        .pipe(gulp.dest(buildFolder));
});


gulp.task('watchers', ['build'], function () {
    'use strict';

    gulp.watch([
        path.join(folders.assets, '**', '*.+(less|css)')
    ], ['process-less']);


    gulp.watch([
        path.join(folders.assets, '**', '*.js')
    ], [
        'process-js-app',
        'process-js-vendor'
    ]);
    gulp.watch('index.html', ['process-index']);
    gulp.watch(path.join(folders.assets, 'images', '**', '*.*'), ['process-images']);
    gulp.watch(path.join(folders.assets, 'fonts', '**', '*.*'), ['process-fonts']);
});


gulp.task('server', function () {
    'use strict';

    var notification = 'Dev server was started';


    return gulp.src(buildFolder)
        .pipe($.plumber(plumberConfig))
        .pipe($.webserver({
            host: 'localhost',
            port: 3333,
            livereload: true,
            fallback: 'index.html'
        }))
        .pipe($.notify(notification));
});

gulp.task('open-browser', ['server'], function () {
    'use strict';
    var notification = 'Browser opened';

    gulp.src(buildFolder + '/index.html')
        .pipe($.open('', {
            url: 'http://localhost:3333',
            app: browserApp
        }))
        .pipe($.notify(notification));

});
